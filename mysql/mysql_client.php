<?php
/************************************************************
 * Copyright (C), 1993, Dacelve. Tech., Ltd.
 * FileName : mysql_client.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/1/17 10:13
 * Description   :
 * Function List :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/1/17   1.0          init
 ***********************************************************/
$sql = 'select * from talk_user where user_id = 5317';
//不使用连接池~耗时6S
$conn = @mysqli_connect("120.24.219.140","admin","admin123",'talk');
if($conn){
    $res=mysqli_query($conn, $sql);
    $row=mysqli_fetch_assoc($res);
    var_dump($row);
}else{
    echo "ERROR";
}

//使用连接池~耗时6S
//$client = new swoole_client(SWOOLE_SOCK_TCP);
//$client->connect('120.24.219.140', 9508, 10) or die("连接失败");
//$client->send($sql);
//$data = $client->recv();//阻塞接受返回的结果
//$client->close();

