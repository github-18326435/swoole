
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <link href="http://chat.workerman.net/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://chat.workerman.net/css/style.css" rel="stylesheet">
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.js"></script>
</head>

<script>
    //回调函数区
    var srcClientId = 0;
    var toClientId = 0;
    var noAllowUserId = 0;
    // var userId = prompt('输入你的用户ID：', '1');
    var userId = Math.floor(Math.random()*99+1);
    // var room = prompt('输入你的房间ID：', '1');
    // var room = 1;
    var room = <?=$_GET['room']?>;
    // var group = prompt('输入你的组别ID：', '1');
    var group = 1;

    var sprintf = $.fn.bootstrapTable.utils.sprintf;
    var loginHtml = '<div class="speech_item">' +
        '<img width="45px" src="%s" class="user_icon">%s<br> %s' +
        '<div style="clear:both;"></div>' +
        '<p class="triangle-isosceles top">%s 加入了聊天室</p></div>';
    var msgHtml = '<div class="speech_item">' +
        '<img width="45px" src="%s" class="user_icon">%s<br> %s' +
        '<div style="clear:both;"></div>' +
        '<p class="triangle-isosceles top">%s</p></div>';
    var msgHtmlTo = '<div class="speech_item">' +
        '<img width="45px" src="%s" class="user_icon">%s<br> %s' +
        '<div style="clear:both;"></div>' +
        '<p class="triangle-isosceles top" style="background: antiquewhite !important;">%s</p></div>';
    var userListHtml = '<li id="user_%s" data-id="%s" data-head="%s" data-clientId="%s" style="cursor: pointer" onclick="chooseUser(this.id)">%s</li>';

    function onLogin(data) {
        console.log('onLogin');
        console.log(data);
        if(userId == data.userId){
            srcClientId = data.clientId;
        }
        //加入聊天室
        var msg = sprintf(loginHtml, data.headImg, getNow(), data.userName, data.userName);
        $('#dialog').append(msg);

        // onUserList(data);
    }
    function onLogout(data) {
        console.log('onLogout');
        console.log(data);

        //删除用户列表
        $('#user_'+data.userId).remove();
    }
    //收到消息后的回调函数
    function onMsg(data) {
        console.log('onMsg');
        console.log(data)
        var msg = sprintf(msgHtml, data.headImg, getNow(), data.userName, data.msg);
        $('#dialog').append(msg);
    }
    function onMsgGroup(data) {
        console.log('onMsgGroup')
        console.log(data)
        var msg = sprintf(msgHtml, data.headImg, getNow(), data.userName, data.msg);
        $('#dialog').append(msg);
    }

    function onMsgTo(data) {
        console.log('onMsgTo');
        console.log(data)
        var msg = sprintf(msgHtmlTo, data.headImg, getNow(), data.userName, data.msg);
        $('#dialog').append(msg);
    }
    function onError(data) {
        console.log('onError')
    }

    function onSpeak(data) {
        console.log('onSpeak')
        console.log(data)
        alert(data.msg);
    }

    function onGift(data) {
        console.log('onGift')
    }

    function onKick(data) {
        console.log('onKick')
        console.log(data)
        alert(data.msg);
        closeWindows();
    }
    
    function onUserList(data) {
        console.log('onUserList');
        console.log(data);
        if(data.data != undefined){
            //初次进入页面全部加载
            $.each(data.data, function (index,value) {
                var t = value.value;
                var msg = sprintf(userListHtml, t.userId, t.userId, t.headImg, t.clientId, t.userName);
                $('#userlist').append(msg);
            });
        }else{
            var msg = sprintf(userListHtml, data.userId, data.userId, data.headImg, data.clientId, data.userName);
            $('#userlist').append(msg);
        }
    }

    function chooseUser(id) {
        noAllowUserId = $('#'+ id).attr('data-id');
        toClientId = $('#'+ id).attr('data-clientId');
    }

    function onAd(data) {
        console.log('onAd')
        console.log(data)
        alert(data.msg)
    }
</script>
<script>
    //发送消息区

    //发送消息(广播、单播、组播)
    function sendMsg(msg, type) {
        var clientMsg = new ClientMsg();
        clientMsg.srcClientId = srcClientId;
        clientMsg.toClientId = toClientId;
        clientMsg.userId = userId;
        clientMsg.room = room;
        clientMsg.group = group;
        clientMsg.msg = msg;
        clientMsg.type = type;
        clientMsg.data = '';
        console.log('发送：');
        console.log(clientMsg);
        ws.send(JSON.stringify(clientMsg));
    }

    function sendMsgAd(msg) {
        var clientMsg = new ClientAd();
        clientMsg.msg = msg;
        clientMsg.data = ['这里可以附加一些数据'];
        ws.send(JSON.stringify(clientMsg));
    }

    //禁言
    function sendSpeak() {
        var clientMsg = new ClientForbiddenSpeak();
        clientMsg.srcClientId = srcClientId;
        clientMsg.toClientId = toClientId;
        clientMsg.notAllowUserId = noAllowUserId;
        clientMsg.room = room;
        clientMsg.group = group;
        clientMsg.data = 1;
        clientMsg.time = 60*60*24;
        console.log('发送：');
        console.log(clientMsg);
        ws.send(JSON.stringify(clientMsg));
    }

    //礼物
    function sendGift() {
        var clientMsg = new ClientSendGift();
        clientMsg.srcClientId = srcClientId;
        clientMsg.toClientId = toClientId;
        clientMsg.userId = userId;
        clientMsg.room = room;
        clientMsg.group = group;
        clientMsg.data = '鲜花';
        clientMsg.number = 1;
        ws.send(JSON.stringify(clientMsg));
    }

    //踢出
    function sendKick(msg) {
        var clientMsg = new ClientKick();
        clientMsg.srcClientId = srcClientId;
        clientMsg.toClientId = toClientId;
        clientMsg.userId = userId;
        clientMsg.room = room;
        clientMsg.group = group;
        clientMsg.data = '';
        ws.send(JSON.stringify(clientMsg));
    }
</script>
<script src="client_global.js" type="text/javascript"></script>

<body onload="connect();">
<div class="container">
    <div class="row clearfix">
        <div class="col-md-1 column">
        </div>
        <div class="col-md-6 column">
            <div class="thumbnail">
                <div class="caption" id="dialog">

                </div>
            </div>
            <form onsubmit=" return false;">
<!--                <select style="margin-bottom:8px" id="client_list">-->
<!--                    <option value="all">所有人</option>-->
<!--                </select>-->
                <textarea class="textarea thumbnail" id="textarea"></textarea>
                <div class="say-btn">
                    <input type="submit" onclick="sendMsgAd(document.getElementById('textarea').value)" class="btn btn-default" value="全站公告" />
                    <input type="submit" onclick="sendMsg(document.getElementById('textarea').value, 0)" class="btn btn-default" value="房间内广播" />
                    <input type="submit" onclick="sendMsg(document.getElementById('textarea').value, 1)" class="btn btn-default" value="房间内组播" />
                    <input type="submit" onclick="sendMsg(document.getElementById('textarea').value, 2)" class="btn btn-default" value="互播" />
                    <input type="submit" onclick="sendSpeak()" class="btn btn-default" value="禁言" />
                    <input type="submit" onclick="sendGift()" class="btn btn-default" value="送礼" />
                    <input type="submit" onclick="sendKick()" class="btn btn-default" value="踢出" />
                </div>
            </form>
<!--            <div>-->
<!--                &nbsp;&nbsp;&nbsp;&nbsp;<b>房间列表:</b>（当前在&nbsp;房间--><?php //echo isset($_GET['room_id'])&&intval($_GET['room_id'])>0 ? intval($_GET['room_id']):1; ?><!--）<br>-->
<!--                &nbsp;&nbsp;&nbsp;&nbsp;<a href="/?room_id=1">房间1</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="/?room_id=2">房间2</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="/?room_id=3">房间3</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="/?room_id=4">房间4</a>-->
<!--                <br><br>-->
<!--            </div>-->
        </div>
        <div class="col-md-3 column">
            <div class="thumbnail">
                <div class="caption">
                    <h4>在线用户</h4>
                    <span style="color: red;font-size: 10px;">点击用户进行、互播、禁言、送礼、踢出</span>
                    <ul></ul>
                    <div id="userlist">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
