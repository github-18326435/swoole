<?php

use app\Redis;

/************************************************************
 * Copyright (C), 1993, Dacelve. Tech., Ltd.
 * FileName : server.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/1/15 9:47
 * Description   :命令行启动本文件作为服务器端
 * Function List :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/1/15   1.0          init
 ***********************************************************/
require __DIR__.'/../app/Server.php'; //协议包应用类
require __DIR__.'/../app/Redis.php'; //Redis应用类

//可承载1000连接左右（最高3800，待优化到3000人）

//用户信息表
$userTable = new swoole_table(1024);
$userTable->column('cmd', swoole_table::TYPE_STRING, 64);
$userTable->column('userId', swoole_table::TYPE_INT, 64);
$userTable->column('room', swoole_table::TYPE_INT, 64);
$userTable->column('group', swoole_table::TYPE_INT, 4);
$userTable->column('userName', swoole_table::TYPE_STRING, 64);
$userTable->column('headImg', swoole_table::TYPE_STRING, 128);
$userTable->column('clientId', swoole_table::TYPE_INT, 4);
$userTable->column('type', swoole_table::TYPE_INT, 4);
$userTable->create();

//Server::daemonize();//切换为守护进程

//配置数组
$config = [
    'heartbeat_check_interval' => 10,
    'heartbeat_idle_time' => 20,
    'ssl_cert_file' => '/usr/local/nginx/conf/cert/www_phpyd_com.crt',
    'ssl_key_file' => '/usr/local/nginx/conf/cert/www_phpyd_com.key',
    'log_file' => 'swoole.log',
    'worker_num' => 1
    //    'daemonize ' => 1
];
//创建websocket服务器对象
$server = new swoole_websocket_server('0.0.0.0', 9501, SWOOLE_BASE, SWOOLE_SOCK_TCP | SWOOLE_SSL);
$server->set($config);

$server->userTable = $userTable;
//开始
$server->on('open', function (swoole_websocket_server $server, $request) {
    //定时任务：检测心跳
    $server->tick(10000, function () use ($server) {
        $closeFdArr = $server->heartbeat(false);
        foreach ($closeFdArr as $fd) {
            $server->close($fd);
            var_dump($fd);
            $server->userTable->del($fd);
        }
    });
    //初始化应用类
    Server::__init($server);
});
//收到客户端消息
$server->on('message', function (swoole_websocket_server $server, $frame) {
    var_dump($frame->data);

});
//关闭连接
$server->on('close', function ($server, $fd) {
//    echo 'close'."\n";
    $userInfo = $server->userTable[$fd];
    $server->after(1000, function () use ($server, $userInfo, $fd) {
        Server::sendLoginToRoom([
            'cmd' => 'logout',
            'msg' => $userInfo['userName'],
            'userId' => $userInfo['userId'],
            'srcClientId' => $fd,
            'toClientId' => 0,
            'room' => $userInfo['room'],
            'group' => $userInfo['group'],
            'type' => $userInfo['type'],
            'data' => [],
        ], $userInfo['room']);
        //记录登陆状态
        if($userInfo['type'] == 1){
            Redis::$dbindex = 2;
            Redis::set('swoole-login-status-'.$userInfo['userId'], 'logout');
        }
    });

    //删除信息
    $server->userTable->del($fd);
});
//服务器启动
$server->start();
